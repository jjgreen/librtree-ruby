/*
  rtree/postscript.h
  Copyright (c) J.J. Green 2020
*/

#ifndef RTREE_POSTSCRIPT_H
#define RTREE_POSTSCRIPT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <rtree/extent.h>

#include <stdio.h>

typedef enum {
  model_none,
  model_grey,
  model_rgb,
  model_cmyk
} postscript_colour_model_t;

typedef struct {
  postscript_colour_model_t model;
  union {
    float grey[1];
    float rgb[3];
    float cmyk[4];
  };
} postscript_colour_t;

typedef struct {
  struct {
    postscript_colour_t colour;
  } fill;
  struct {
    postscript_colour_t colour;
    float width;
  } stroke;
} postscript_style_level_t;

typedef struct {
  size_t n;
  postscript_style_level_t *array;
} postscript_style_t;

postscript_style_t* postscript_style_read(FILE*);
void postscript_style_destroy(postscript_style_t*);

typedef struct rtree_postscript_t
{
  const postscript_style_t *style;
  extent_axis_t axis;
  float extent;
  float margin;
  const char *title;
} rtree_postscript_t;

#ifdef __cplusplus
}
#endif

#endif
