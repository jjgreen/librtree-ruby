describe 'RTree' do
  describe '.version' do

    it 'responds' do
      expect(RTree).to respond_to :version
    end

    subject { RTree.version }

    it 'does not raise' do
      expect { subject }.to_not raise_error
    end

    it 'is an array' do
      expect(subject).to be_an Array
    end

    it 'has length 3' do
      expect(subject.length).to eq 3
    end

    it 'consists of integers' do
      subject.each { |item| expect(item).to be_an Integer }
    end

    it 'has major version at least 1' do
      expect(subject.first).to be >= 1
    end
  end
end
