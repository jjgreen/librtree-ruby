describe 'RTree::Style' do

  describe '.json_read' do

    let(:tempfile) { Tempfile.new('json-read-spec') }
    let(:path) { tempfile.path }
    let(:pathname) { Pathname.new(path) }
    let(:json) do
      <<~EOF
      [
        {
          "stroke": {
            "grey": 0.9,
            "width": 1.0
          }
        },
        {
           "fill": {
             "rgb": [0.9, 0.1, 0.1]
           }
        }
      ]
      EOF
    end

    before { File.open(tempfile, 'w') { |io| io.write(json) } }

    shared_examples 'it reads JSON' do

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is an RTree::Style' do
        expect(subject).to be_an_instance_of RTree::Style
      end
    end

    context 'argument an IO' do
      subject do
        File.open(path, 'r') { |io| RTree::Style.json_read(io) }
      end
      it_behaves_like 'it reads JSON'
    end

    context 'argument a String' do
      subject { RTree::Style.json_read(path) }
      it_behaves_like 'it reads JSON'
    end

    context 'argument a Pathname' do
      subject { RTree::Style.json_read(pathname) }
      it_behaves_like 'it reads JSON'
    end
  end
end
