describe 'RTree::Style' do

  describe '.from_a' do

    subject { RTree::Style.from_a(array) }

    context 'valid array' do
      let(:array) do
        [
          {
            stroke: {
              grey: 0.9,
              width: 1.0
            }
          },
          {
             fill: {
               rgb: [0.9, 0.1, 0.1]
             }
          }
        ]
      end

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is an RTree::Style' do
        expect(subject).to be_an_instance_of RTree::Style
      end
    end

    context 'array with no levels' do
      let(:array) { [] }

      it 'raise RuntimeError' do
        expect { subject }.to raise_error RuntimeError
      end
    end

    context 'array with empty levels' do
      let(:array) { [{}, {}] }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is an RTree::Style' do
        expect(subject).to be_an_instance_of RTree::Style
      end
    end
  end
end
