describe 'RTree' do
  describe '#envelope' do
    let(:rtree) { RTree.new(2) }

    subject { rtree.envelope }

    it 'responds' do
      expect(rtree).to respond_to :envelope
    end

    context 'empty' do

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is nil' do
        expect(subject).to be_nil
      end
    end

    context 'non-empty' do
      before do
        nodes.each { |node| rtree.add_rect(*node) }
      end

      context 'containing a single rectangle' do
        let(:rect) { [1, 2, 3, 4] }
        let(:nodes) { [ [1, rect] ] }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is not nil' do
          expect(subject).to_not be_nil
        end

        it 'is an array' do
          expect(subject).to be_an Array
        end

        it 'is the input rectangle' do
          expect(subject).to eq rect
        end
      end

      context 'contains multiple rectangles' do
        let(:nodes) do
          [
            [1, [0, 0, 1, 1]],
            [2, [1, 1, 2, 2]],
            [3, [2, 2, 3, 3]]
          ]
        end

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is not nil' do
          expect(subject).to_not be_nil
        end

        it 'is an array' do
          expect(subject).to be_an Array
        end

        it 'is the expected rectangle' do
          expect(subject).to eq [0, 0, 3, 3]
        end
      end
    end
  end
end
