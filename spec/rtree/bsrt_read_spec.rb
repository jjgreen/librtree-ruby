require 'tempfile'

describe 'RTree' do
  describe '.bsrt_read' do

    it 'responds' do
      expect(RTree).to respond_to(:bsrt_read)
    end

    describe 'bad argument types' do
      subject { RTree.bsrt_read(file) }

      context 'nil' do
        let(:file) { nil }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'number' do
        let(:file) { 123 }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end
    end

    shared_examples 'it reads BSRT' do

      context 'empty tree' do
        let(:nodes) { [] }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end

        it 'is empty' do
          expect(subject).to be_empty
        end
      end

      context 'non-empty tree' do
        let(:nodes) do
          [
            [1, [0, 0, 1, 1]],
            [2, [1, 1, 2, 3]]
          ]
        end

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end

        it 'is not empty' do
          expect(subject).to_not be_empty
        end
      end
    end

    context 'with a dynamic fixture ' do

      # generally one would use fixtures here, but BSRT setialisation
      # is non-portable (across platforms with different page-sizes,
      # for example), so instead we create the BSRT stream by reading
      # the output of #bsrt_write, so these are round-trip tests really.

      let(:tempfile) { Tempfile.new('bsrt-read-spec') }
      let(:path) { tempfile.path }
      let(:pathname) { Pathname.new(path) }
      let(:instance) { RTree.new(2) }

      before do
        nodes.each { |node| instance.add_rect(*node) }
        File.open(tempfile, 'w') { |file| instance.bsrt_write(file) }
      end

      describe 'argument is valid BSRT stream' do
        subject do
          File.open(tempfile, 'r') { |file| RTree.bsrt_read(file) }
        end

        it_should_behave_like 'it reads BSRT'
      end

      describe 'argument is valid BSRT path' do
        subject { RTree.bsrt_read(path) }

        it_should_behave_like 'it reads BSRT'
      end

      describe 'argument is valid BSRT pathname' do
        subject { RTree.bsrt_read(pathname) }

        it_should_behave_like 'it reads BSRT'
      end
    end
  end
end
