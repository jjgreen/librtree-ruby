describe 'RTree' do
  describe '#branching_factor' do
    let(:rtree) { RTree.new(2) }

    it 'responds' do
      expect(rtree).to respond_to :branching_factor
    end

    subject { rtree.branching_factor }

    it 'does not raise' do
      expect { subject.to_not raise_error }
    end

    it 'is an Integer' do
      expect(subject).to be_an Integer
    end

    it 'is postitive' do
      expect(subject).to be > 0
    end
  end
end
