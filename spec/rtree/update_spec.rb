describe 'RTree' do
  describe '#update!' do
    let(:rtree) { RTree.new(2) }
    before do
      [
        [1, [0, 0, 1, 1]],
        [2, [1, 1, 2, 3]]
      ].each { |node| rtree.add_rect(*node) }
    end

    context 'no block' do
      subject { rtree.update! }

      it 'raises' do
        expect { subject }.to raise_error ArgumentError
      end
    end

    context 'a block yielding non-arrays' do
      subject do
        rtree.update! { |id, coord| 'quack' }
      end

      it 'raises' do
        expect { subject }.to raise_error TypeError
      end
    end

    context 'a block yielding arrays of the wrong size' do
      subject do
        rtree.update! { |id, coord| [0, 0, 0] }
      end

      it 'raises' do
        expect { subject }.to raise_error ArgumentError
      end
    end

    context 'a block yielding arrays with elements of the wrong type' do
      subject do
        rtree.update! { |id, coord| [0, 0, 0, 'quack'] }
      end

      it 'raises' do
        expect { subject }.to raise_error TypeError
      end
    end

    context 'a block yielding the same arrays' do
      subject do
        rtree.update! { |id, coord| coord }
      end

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'does not change the hash representation' do
        expect { subject }.to_not change { rtree.to_h }
      end
    end

    context 'a block yielding shifted arrays' do
      subject do
        rtree.update! { |id, coord| coord.map { |x| x + 1 } }
      end

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'changes the hash representation' do
        expect { subject }.to change { rtree.to_h }
      end
    end
  end
end
