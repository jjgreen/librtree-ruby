describe 'RTree' do
  describe '.new' do

    context 'without keyword argument' do
      subject { RTree.new(*arg) }

      context 'no argument' do
        let(:arg) { [] }

        it 'raise ArgumentError' do
          expect { subject }.to raise_error ArgumentError
        end
      end

      context 'string argument' do
        let(:arg) { ['meh'] }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'sensible dimension argument' do
        let(:arg) { [2] }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end
      end
    end

    context 'with keyword argument' do
      subject { RTree.new(dim, **kwarg) }
      let(:dim) { 2 }

      context 'empty' do
        let(:kwarg) { {} }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end
      end

      context 'split linear' do
        let(:kwarg) { { split: :linear } }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end
      end

      context 'split quadratic' do
        let(:kwarg) { { split: :quadratic } }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end
      end

      context 'split greene' do
        let(:kwarg) { { split: :greene } }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end
      end

      context 'split cubic' do
        let(:kwarg) { { split: :cubic } }

        it 'raises' do
          expect { subject }
            .to raise_error(ArgumentError, /bad split/)
        end
      end

      context 'node_page' do
        let(:kwarg) { { node_page: 16 } }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end
      end

      context 'unknown keyword' do
        let(:kwarg) { { meh: 5 } }

        it 'raises' do
          expect { subject }
            .to raise_error(ArgumentError, /unknown keyword/)
        end
      end
    end
  end
end
