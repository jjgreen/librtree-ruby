describe 'RTree' do
  describe '#clone' do
    subject { rtree.clone }
    let(:rtree) { RTree.new(2) }
    before { nodes.each { |node| rtree.add_rect(*node) } }

    context 'empty' do
      let(:nodes) { [] }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is the same size' do
        expect(subject.size).to eq rtree.size
      end

      it 'is identical' do
        expect(subject).to eq rtree
      end
    end

    context 'non-empty' do
      let(:nodes) do
        [
          [1, [0, 0, 1, 1]],
          [2, [1, 1, 2, 3]]
        ]
      end

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is the same size' do
        expect(subject.size).to eq rtree.size
      end

      it 'is identical' do
        expect(subject).to eq rtree
      end
    end
  end
end
