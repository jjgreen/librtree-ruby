describe 'RTree' do
  describe '#dim' do
    let(:rtree) { RTree.new(dim) }

    subject { rtree.dim }

    context 'dim 2' do
      let(:dim) { 2 }

      it 'responds' do
        expect(rtree).to respond_to :dim
      end

      it 'does not raise' do
        expect { subject.to_not raise_error }
      end

      it 'is 2' do
        expect(subject).to eq dim
      end
    end

    context 'dim 10' do
      let(:dim) { 10 }

      it 'does not raise' do
        expect { subject.to_not raise_error }
      end

      it 'is 10' do
        expect(subject).to eq dim
      end
    end
  end
end
