require 'json'
require 'stringio'
require 'tempfile'

describe 'RTree' do
  describe '#json_write' do
    let(:rtree) { RTree.new(2) }
    let(:nodes) { [] }

    before do
      nodes.each { |node| rtree.add_rect(*node) }
    end

    it 'responds' do
      expect(rtree).to respond_to :json_write
    end

    describe 'argument types' do
      subject { rtree.json_write(file) }

      context 'nil' do
        let(:file) { nil }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'number' do
        let(:file) { 23456 }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'read-only file' do
        let(:file) { File.open('/dev/null', 'r') }

        it 'raises IOError' do
          expect { subject }.to raise_error IOError
        end
      end

      context 'writeable file' do
        let(:file) { File.open('/dev/null', 'w') }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'returns an RTree' do
          expect(subject).to be_an RTree
        end
      end
    end

    describe 'output' do
      let(:tempfile) { Tempfile.new('json-write-spec') }
      let(:path) { tempfile.path }
      let(:pathname) { Pathname.new(path) }

      after { FileUtils.rm_f path }

      shared_examples 'it writes JSON' do

        it 'exists' do
          expect(File.exist? path).to be true
        end

        describe 'content' do
          let(:content) { File.read(path) }

          it 'does not raise' do
            expect { content }.to_not raise_error
          end

          it 'is not empty' do
            expect(content).to_not be_empty
          end

          it 'can be parsed as JSON' do
            expect { JSON.parse(content) }.to_not raise_error
          end

          describe 'parsed as JSON' do
            let(:parsed) { JSON.parse(content,  symbolize_names: true) }
            let(:expected_state) do
              {
                dims: 2,
                page_size: Integer,
                float_size: Integer,
                rect_size: Integer,
                branch_size: Integer,
                node_size: Integer
              }
            end

            context 'with an empty tree' do

              it 'is the expected hash' do
                expected_hash = {
                  root: {
                    level: 0,
                    count: 0,
                    branches: []
                  },
                  state: expected_state
                }
                expect(parsed).to match expected_hash
              end
            end

            context 'with an non-empty tree' do

              let(:nodes) do
                [
                  [1, [0, 0, 1, 1]],
                  [2, [1, 1, 2, 3]]
                ]
              end

              it 'is the expected hash' do
                expected_hash = {
                  root: {
                    level: 0,
                    count: 2,
                    branches: [
                      {
                        id: 1,
                        rect: [0.0, 0.0, 1.0, 1.0]
                      },
                      {
                        id: 2,
                        rect: [1.0, 1.0, 2.0, 3.0]
                      }
                    ]
                  },
                  state: expected_state
                }
                expect(parsed).to match expected_hash
              end
            end
          end
        end
      end

      describe 'argument is an IO' do
        before do
          File.open(path, 'w') { |io| rtree.json_write(io) }
        end
        it_should_behave_like 'it writes JSON'
      end

      describe 'argument is a String' do
        before { rtree.json_write(path) }
        it_should_behave_like 'it writes JSON'
      end

      describe 'argument is a Pathname' do
        before { rtree.json_write(path) }
        it_should_behave_like 'it writes JSON'
      end
    end
  end
end
