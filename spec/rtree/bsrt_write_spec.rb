require 'stringio'
require 'tempfile'

describe 'RTree' do
  describe '#bsrt_write' do
    let(:rtree) { RTree.new(2) }
    let(:nodes) { [] }

    before do
      nodes.each { |node| rtree.add_rect(*node) }
    end

    it 'responds' do
      expect(rtree).to respond_to :bsrt_write
    end

    describe 'argument types' do
      subject { rtree.bsrt_write(file) }

      context 'nil' do
        let(:file) { nil }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'number' do
        let(:file) { 12345 }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'read-only file' do
        let(:file) { File.open('/dev/null', 'r') }

        it 'raises IOError' do
          expect { subject }.to raise_error IOError
        end
      end

      context 'writeable file' do
        let(:file) { File.open('/dev/null', 'w') }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'returns an RTree' do
          expect(subject).to be_an RTree
        end
      end
    end

    describe 'output' do
      let(:tempfile) { Tempfile.new('bsrt-write-spec') }
      let(:path) { tempfile.path }
      let(:pathname) { Pathname.new(path) }

      after { FileUtils.rm_f path }

      shared_examples 'it writes BSRT' do

        it 'exists' do
          expect(File.exist? path).to be true
        end

        describe 'content' do
          let(:content) { File.read(path) }

          it 'does not raise' do
            expect { content }.to_not raise_error
          end

          it 'is not empty' do
            expect(content).to_not be_empty
          end

          it 'has the BSRt magic' do
            expect(content.byteslice(0, 4)).to eq 'BSRt'
          end
        end
      end

      describe 'argument is an IO' do
        before do
          File.open(path, 'wb') { |io| rtree.bsrt_write(io) }
        end
        it_should_behave_like 'it writes BSRT'
      end

      describe 'argument is a String' do
        before { rtree.bsrt_write(path) }
        it_should_behave_like 'it writes BSRT'
      end

      describe 'argument is a Pathname' do
        before { rtree.bsrt_write(path) }
        it_should_behave_like 'it writes BSRT'
      end
    end
  end
end
