describe 'RTree' do
  describe '#node_size' do
    let(:rtree) { RTree.new(2) }

    it 'responds' do
      expect(rtree).to respond_to :node_size
    end

    subject { rtree.node_size }

    it 'does not raise' do
      expect { subject.to_not raise_error }
    end

    it 'is an Integer' do
      expect(subject).to be_an Integer
    end

    it 'agrees with #to_h' do
      expect(subject).to eq rtree.to_h[:state][:node_size]
    end
  end
end
