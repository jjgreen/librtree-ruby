Changelog
---------

### 1.0.6, 07-04-2024

- Update to librtree 1.3.2
- Adds `#envelope` `#csv_write`, `#to_csv`

### 1.0.5, 16-01-2024

- All methods which take a `File` object (i.e., a stream), now
  also accept a `String` or `Pathname` to the file instead.

### 1.0.4, 30-10-2023

- Check for `__builtin_ctzl` (and use if found)

### 1.0.3, 16-10-2023

- Update to librtree 1.3.0
- Remove workaround for "optional argument to #exit!" issue

### 1.0.2, 06-04-2023

- The `#serialise` method raises if the read from pipe returns
  nil (which it might), so the method (and other methods which
  call it to serialise) now return `String` or raise
- Added RBS static type signatures and infrastructure for static
  type checking with **steep**

### 1.0.1, 16-03-2023

- Update to 1.1.4 of embedded `librtree`
- The `#empty?` method uses faster `rtree_empty` of the latest
  version of the embedded library
- Added the `simplecov` gem and configured (Ruby) coverage in
  CI runs

### 1.0.0, 24-02-2023

- The `librtree` library is no longer a requirement: the code
  for the library is it is embedded in the gem itself

### 0.9.1, 21-01-2023

- Tidying
- Fix warning "undefining the allocator of T_DATA class" on
  Ruby 3.2
- No functional changes

### 0.9.0, 07-11-2021

- Code reorganisation
- RuboCop style fixes
- No functional changes

### 0.8.9, 24-08-2021

- Unset `O_NONBLOCK` on `IO.pipe` readers, to allow the gem to
  work with Ruby 3

### 0.8.8, 14-07-2021

- De-nest C base-class for neater documentation
- Adopt "standard" naming-convention for the benfit of Yard
  introspection
- No functional changes

### 0.8.7, 24-06-2021

- Added the `#postscript` method, for plotting

### 0.8.6, 17-06-2021

- Support for the Greene-split of librtree 1.0.6

### 0.8.5, 13-06-2021

- Removed check for `libcsv`, needs librtee version 1.0.5

### 0.8.4, 24-05-2021

- Replace deprecated `Data_*_Struct` API functions
- Implement the `version`, `bugreport`, `url` class methods
- Add `dsize` to the base `rb_data_type_t` struct (using a call to
  the C `rtree_bytes` function introduced in librtree 1.0.3)
- Add the `#size` methods using the same C function

### 0.8.3, 23-05-2021

- Fix a memory leak
- C code uses `Data_Wrap_Struct` rather than `Data_Make_Struct`,
  this needs librtree version at least 1.0.2

### 0.8.2, 22-05-2021

- Added a number of read-accessors for the R-tree state

### 0.8.1, 19-05-2021

- Added changelog
- Updated user documentation
- No code changes

### 0.8.0, 17-05-2021

- First public release
